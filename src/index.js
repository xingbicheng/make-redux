// 它存在一个重大的隐患，我们渲染数据的时候，
// 使用的是一个共享状态 appState，每个人都可以修改它。
// 出现问题的时候debug很困难，所以应该避免使用全局变量

function renderApp (newState, oldState = {}) { // 防止未传oldState，给其设置默认值
  console.log(newState === oldState)
  if (newState === oldState) {
    return
  }
  console.log('render app...')
  renderTitle(newState.title, oldState.title)
  renderContent(newState.content, oldState.content) // 我们并没有想修改content，可是这里却render了一遍content
}

function renderTitle (newTitle, oldTitle = {}) {
  if (newTitle === oldTitle) {
    return
  }
  console.log('render title...')
  const titleDOM = document.getElementById('title')
  titleDOM.innerHTML = newTitle.text
  titleDOM.style.color = newTitle.color
}

function renderContent (newContent, oldContent = {}) {
  if (newContent === oldContent) {
    return
  }
  console.log('render content...')
  const contentDOM = document.getElementById('content')
  contentDOM.innerHTML = newContent.text
  contentDOM.style.color = newContent.color
}


//如果某个函数修改了 title.text 但是我并不想要它这么干，我需要 debug 出来是哪个函数修改了，
// 我只需要在 dispatch的 switch 的第一个 case 内部打个断点就可以调试出来了。
// 下面函数只是一个构造新的state的纯函数
function themeReducer (state, action) { // 我们定义一个函数，修改全局变量时必须调用它，提高了修改全局变量的门槛
  if (!state) {
    return { // 返回初始数据，此部分代码是为了初始化state
      title: {
        text: 'React.js 小书',
        color: 'red',
      },
      content: {
        text: 'React.js 小书内容',
        color: 'blue'
      }
    }
  }
  switch (action.type) {
    case 'UPDATE_TITLE_TEXT':// 可以在这里打个断点，可以很快查到是谁调用dispatch修改了text
      return { // 返回一个浅复制的对象，用来分辨state是否变化
        ...state,
        title: {
          ...state.title,
          text: action.text
        }
      }
    case 'UPDATE_TITLE_COLOR':
      console.log({
        ...state,
        title: {
          ...state.title,
          color: action.color
        }
      })
      return {
        ...state,
        title: {
          ...state.title,
          color: action.color
        }
      }
    default:
      return state
  }
}
function createStore(reducer) { // reducer只是一个构造新的state的一个纯函数
  let state = null
  const listeners = []
  const subscribe = (listener) => { // 添加监听函数
    listeners.push(listener)
  }
  const getState = () => state;
  const dispatch = (action) => { // 调用dispatch后自动执行listener监听函数
    state = reducer(state, action) // 覆盖原对象
    console.log(state)
    listeners.forEach((listener) => { // 观察者模式
      listener()
    })
  }
  dispatch({})
  return {
    getState,
    dispatch,
    subscribe
  }
}

const store = createStore(themeReducer) // 传入初始化数据state
let oldState = store.getState()
store.subscribe(() => {
  const newState = store.getState()
  console.log(newState, oldState)
  console.log(newState === oldState)
  renderApp(newState, oldState)
  oldState = newState // render之后newState就变成了oldState
}) // 添加监听函数
renderApp(store.getState())
store.dispatch({ type: 'UPDATE_TITLE_TEXT', text: '《React.js 小书》' }) // 修改标题
store.dispatch({ type: 'UPDATE_TITLE_COLOR', color: 'green' }) // 修改标题颜色